# syntax=docker/dockerfile:1
FROM jelastic/nodejs:12.18.4-pm2
WORKDIR /app
COPY src ./src
# COPY node_modules ./node_modules
COPY nuxt.config.js package-lock.json package.json ./
RUN npm install
EXPOSE 8080
CMD ["npm", "run", "serve"]