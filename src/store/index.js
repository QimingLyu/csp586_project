import config from '@/config.js'

const debug = process.env.NODE_ENV !== 'production'
export const strict = debug

export const state = () => ({
    misc: null,
    loading: 0,
    msgs: [],
    messageId: 1,
    online: 0,
    _initing: false,

    config: {
        remote: config.remote,
        ws: { enable: false },

        siteName: 'Icarus',
        title: 'Icarus',
        logoText: 'Icarus'
    }
})

export const getters = {
    misc: (state) => state.misc || {},
    BACKEND_CONFIG: (state, getters) => getters.misc.BACKEND_CONFIG || {},
    POST_TYPES: (state, getters) => getters.misc.POST_TYPES || {},
    POST_TYPES_TXT: (state, getters) => getters.misc.POST_TYPES_TXT || {},
    POST_STATE: (state, getters) => getters.misc.POST_STATE || {},
    POST_STATE_TXT: (state, getters) => getters.misc.POST_STATE_TXT || {},
    POST_VISIBLE: (state, getters) => getters.misc.POST_VISIBLE || {},
    POST_VISIBLE_TXT: (state, getters) => getters.misc.POST_VISIBLE_TXT || {},
    MANAGE_OPERATION: (state, getters) => getters.misc.MANAGE_OPERATION || {},
    MANAGE_OPERATION_TXT: (state, getters) => getters.misc.MANAGE_OPERATION_TXT || {},
    USER_GROUP: (state, getters) => getters.misc.USER_GROUP || {},
    USER_GROUP_TXT: (state, getters) => getters.misc.USER_GROUP_TXT || {},
    USER_GROUP_TO_ROLE: (state, getters) => getters.misc.USER_GROUP_TO_ROLE || {},
    NOTIF_TYPE: (state, getters) => getters.misc.NOTIF_TYPE || {},

    isInited: (state) => (!state._initing) && (state.misc),
    isAboutPageEnable: (state, getters) => getters.BACKEND_CONFIG.ABOUT_PAGE_ENABLE,
    isSearchEnable: (state, getters) => getters.BACKEND_CONFIG.SEARCH_ENABLE
}

export const mutations = {
    SET_INITING (state, data) {
        state._initing = data
    },
    SET_MISC (state, data) {
        state.misc = data
        state.config.siteName = data.BACKEND_CONFIG.SITE_NAME
        state.config.title = data.BACKEND_CONFIG.SITE_TITLE_TEXT
        state.config.logoText = data.BACKEND_CONFIG.SITE_LOGO_TEXT
        this.$api.retcode = data.retcode
        this.$api.retinfo = data.retinfo_cn
    },
    LOADING_INC (state, num = 1) {
        if (process.browser) {
            state.loading += num
        }
    },
    LOADING_DEC (state, num = 1) {
        if (process.browser) {
            state.loading -= num
        }
    },
    LOADING_SET (state, num = 0) {
        if (process.browser) {
            state.loading = num
        }
    },
    // MESSAGE
    MESSAGE_PUSH (state, info) {
        info.id = state.messageId++
        state.msgs.push(info)
    },
    MESSAGE_REMOVE (state, info) {
        state.msgs.splice(state.msgs.indexOf(info), 1)
    },
    SET_ONLINE (state, num) {
        state.online = num
    }
}

export const actions = {
    async initLoad ({ state, commit, dispatch }, payload) {
        if (state._initing) return
        commit('SET_INITING', true)
        let ret = await $.retryUntilSuccess(this.$api.misc)
        if (ret.code !== this.$api.retcode.SUCCESS) {
            this.$message.error(`server error。`, 5000)
            commit('SET_INITING', false)
            return
        }
        commit('SET_MISC', ret.data)

        if (ret.data.user) {
            let miscUser = ret.data.user
            await dispatch('user/apiGetUserData', miscUser.id)
        } else {
            commit('user/RESET')
        }
        // $.tickStart()
        commit('SET_INITING', false)
    },
    async tryInitLoad ({ state, dispatch }) {
        if (!(state.misc || state._initing)) {
            await dispatch('initLoad')
        }
    },
    async nuxtServerInit ({ commit, dispatch }, { req, app }) {
        // 设置初始 access token
        // this.$api.accessToken = this.$storage.getUniversal('t')
        // 尝试加载
        await dispatch('tryInitLoad')

    }
}
