import Color from 'color'

export const state = () => ({
    loaded: false,
    lst: [],
    rawLst: [],
    infoMap: {},
    exInfoMap: {}
})

export const getters = {
    isSiteNew: (state) => state.rawLst.length === 0
}

export const mutations = {
    SET_BOARD_MANY_INFO (state, { lst, rawLst, infoMap }) {
        state.lst = lst
        state.rawLst = rawLst
        state.infoMap = infoMap
    },
    SET_BOARD_EXINFO (state, { id, data }) {
        state.exInfoMap[id] = data
    },
    SET_BOARD_EXINFO_CHAIN (state, { id, data }) {
        if (!state.exInfoMap[id]) {
            state.exInfoMap[id] = {}
        }
        state.exInfoMap[id].chain = data
    },
    SET_LOADED (state, val) {
        state.loaded = val
    }
}

export const actions = {
    fetchBoardChain ({ state, commit }, { boardId }) {
        if (!state.loaded) {
            return []
        }

        let theLst = ((boardId) => {
            let lst = [boardId]
            if (!boardId) return lst
            let infoMap = state.infoMap
            if (!Object.keys(infoMap).length) return lst
            while (true) {
                if (!infoMap[boardId]) {
                    return []
                }
                let pid = infoMap[boardId].parent_id
                if (!pid) break
                lst.push(pid)
                boardId = pid
            }
            return lst
        })(boardId)

        commit('SET_BOARD_EXINFO_CHAIN', { 'id': boardId, 'data': theLst })
    },
    async load ({ state, commit, dispatch }, forceRefresh = false) {
        if (state.loaded && (!forceRefresh)) return
        let boards = await this.$api.board.list({
            order: 'parent_id.desc,weight.desc,time.asc'
        })

        if (boards.code === this.$api.retcode.SUCCESS) {
            let lst = []
            let infoMap = {}

            for (let i of boards.data.items) {
                let subboards = []
                for (let j of boards.data.items) {
                    if (j.parent_id === i.id) subboards.push(j)
                }

                infoMap[i.id] = i
                if (!i.parent_id) {
                    lst.push(i)
                }

                let color = $.boardColor(i)
                commit('SET_BOARD_EXINFO', {
                    'id': i.id,
                    'data': {
                        'subboards': subboards,
                        'subboardsAll': [],
                        'color': color,
                        // darken 10% when hover
                        'colorHover': Color(color).darken(0.1).string()
                    }
                })
            }

            commit('SET_BOARD_MANY_INFO', { lst, infoMap, rawLst: boards.data.items })
            commit('SET_LOADED', true)

            for (let i of boards.data.items) {
                let exInfo = state.exInfoMap[i.id]
                let func = (subboards) => {
                    for (let j of subboards) {
                        exInfo.subboardsAll.push(j)
                        func(state.exInfoMap[j.id].subboards)
                    }
                }
                func(exInfo.subboards)

                dispatch('fetchBoardChain', { boardId: i.id })
            }

            dispatch('fetchBoardChain', { boardId: undefined })
        }
    }
}
