export const state = () => ({
    topicManage: null,
    topicManageData: null,
    boardManage: null,
    boardManageData: null,
    userManage: null,
    userManageData: null,
    commentManage: null,
    commentManageData: null,
    siteNew: false,
    userSetAvatar: false,
    userSetAvatarData: null,
    userInactive: false,
    userSetNickname: false,
    userSignout: false
})

export const getters = {
}

export const mutations = {
    SET_TOPIC_MANAGE: (state, { val, data }) => {
        state.topicManage = val
        state.topicManageData = data
    },
    SET_BOARD_MANAGE: (state, { val, data }) => {
        state.boardManage = val
        state.boardManageData = data
    },
    SET_USER_MANAGE: (state, { val, data }) => {
        state.userManage = val
        state.userManageData = data
    },
    SET_COMMENT_MANAGE: (state, { val, data }) => {
        state.commentManage = val
        state.commentManageData = data
    },
    SET_USER_AVATAR: (state, { val, data }) => {
        state.userSetAvatar = val
        state.userSetAvatarData = data
    },
    SET_SITE_NEW: (state, { val }) => {
        state.siteNew = val
    },
    SET_USER_INACTIVE: (state, { val }) => {
        state.userInactive = val
    },
    SET_USER_NICKANME: (state, { val }) => {
        state.userSetNickname = val
    },
    SET_USER_SIGNOUT: (state, { val }) => {
        state.userSignout = val
    },
    WRITE_BOARD_MANAGE_DATA: (state, data) => {
        if (state.boardManageData) {
            Object.assign(state.boardManageData, data)
        }
    },
    WRITE_USER_MANAGE_DATA: (state, data) => {
        if (state.userManageData) {
            Object.assign(state.userManageData, data)
        }
    },
    CLOSE_ALL: (state) => {
        state.topicManage = false
        state.boardManage = false
        state.userManage = false
        state.commentManage = false
        state.siteNew = false
        state.userSetAvatar = false
        state.userInactive = false
        state.userSetNickname = false
        state.userSignout = false
    }
}

export const actions = {
}
