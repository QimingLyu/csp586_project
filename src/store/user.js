export const state = () => ({
    userData: null,
    unread: 0
})

export const getters = {
    _userData: (state) => state.userData || {},

    isNewUser: (state, getters) => {
        if (!state.userData) return
        let ts = new Date().getTime() / 1000
        if ((state.userData.is_new_user) && (ts - state.userData.time < 24 * 60 * 60)) {
            return true
        }
    },

    roles: (state, getters) => getters._userData.roles,
    basicRole: (state, getters) => state.userData ? 'user' : null,
    mainRole: (state, getters) => getters._userData['main_role'],
    forumAdminRole: (state, getters) => {
        if (getters.isSiteAdmin) {
            return getters.mainRole
        }
    },
    wikiEditRole: (state, getters) => {
        if (getters.isSiteAdmin) {
            return getters.mainRole
        }
        if (getters._userData.is_wiki_editor) {
            return 'wiki_editor'
        }
    },
    isWikiAdmin: (state, getters) => {
        if (!state.userData) return
        return getters.isSiteAdmin || state.userData.is_wiki_editor
    },
    isSiteAdmin: (state, getters) => ['superuser', 'admin'].indexOf(getters.mainRole) !== -1,
    isForumAdmin: (state, getters) => getters.isSiteAdmin
}

export const mutations = {
    SET_USER_DATA (state, data) {
        state.userData = data
    },
    SET_UNREAD (state, data) {
        state.unread = data
    },
    RESET (state) {
        state.unread = 0
        state.userData = null
    }
}

export const actions = {
    async apiSignout ({ dispatch }) {
        let ret = await this.$api.user.signout()
        if (ret.code === this.$api.retcode.SUCCESS || ret.code === this.$api.retcode.FAILED) {
            await dispatch('initLoad', null, { root: true })
            this.$message.success('登出成功')
        }
    },
    async apiGetUserData ({ state, getters, commit }, uid) {
        if (!uid) uid = getters._userData.id
        if (!uid) return

        let userInfo = await this.$api.user.get({ id: uid }, 'user')
        if (userInfo.code === this.$api.retcode.SUCCESS) {
            commit('SET_USER_DATA', userInfo.data)
        } else {
            this.$message.error('error')
        }
    },
    async apiSetUserData ({ state, getters, dispatch }, newData) {
        let oldData = state.userData
        let updateData = $.objDiff(newData, oldData)
        delete updateData['avatar']
        if (Object.keys(updateData).length === 0) return
        let ret = await this.$api.user.set({ id: oldData.id }, updateData, 'user')
        if (ret.code === this.$api.retcode.SUCCESS) {
            await dispatch('apiGetUserData')
            this.$message.success('update succesfully！')
        }
    }
}
