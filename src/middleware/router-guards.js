export default async function ({ store, route, redirect, req }) {
    store.commit('dialog/CLOSE_ALL')
    await store.dispatch('tryInitLoad')
}
