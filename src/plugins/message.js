
if (process.browser) {
    window.onNuxtReady(async (ctx) => {
        let store = ctx.$store
        for (let i of store.state.msgs) {
            _.delay(() => {
                store.commit('MESSAGE_REMOVE', i)
            }, 3000)
        }
    })
}

function createMessageBoard (ctx) {
    class MessageBoard {
        text (text, type = 'default', timeout = 3000) {
            // type: default, secondary, success, warning, error
            let convert = {
                'default': '',
                'secondary': 'am-alert-secondary',
                'success': 'am-alert-success',
                'warning': 'am-alert-warning',
                'error': 'am-alert-danger'
            }
            let data = { type, text, class: convert[type] }
            if (ctx.store) {
                ctx.store.commit('MESSAGE_PUSH', data)
                if (process.browser) {
                    _.delay(() => {
                        ctx.store.commit('MESSAGE_REMOVE', data)
                    }, timeout)
                }
            }
        }

        secondary (text, timeout = 3000) {
            this.text(text, 'secondary', timeout)
        }

        success (text, timeout = 3000) {
            this.text(text, 'success', timeout)
        }

        warn (text, timeout = 3000) {
            this.text(text, 'warning', timeout)
        }

        error (text, timeout = 3000) {
            this.text(text, 'error', timeout)
        }

        byCode (code, data, text = null, timeout = 3000) {
            text = text || ctx.store.state.misc.retinfo_cn[code]
            if (code === ctx.store.state.misc.retcode.SUCCESS) this.success(text, timeout)
            else if (code === ctx.store.state.misc.retcode.TOO_FREQUENT && data) {
                this.error(`${text}，need to wait ${data} seconds`, timeout)
            } else this.error(text, timeout)
        }

        byForm (code, data, alias, timeout = 6000) {
            if (code) {
                for (let [k, errs] of Object.entries(data)) {
                    for (let err of errs) {
                        let name = alias[k] || k
                        this.byCode(code, data, `${name}：${err}`, timeout)
                    }
                }
            } else {
                this.byCode(code, data, null, timeout)
            }
        }
    }

    return new MessageBoard()
}

export default (ctx, inject) => {
    inject('message', createMessageBoard(ctx))
}
