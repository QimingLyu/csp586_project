import Vue from 'vue'
import Router from 'vue-router'

import AccountSignin from '@/views/account/signin.vue'
import AccountSignup from '@/views/account/signup.vue'
import AccountSignupByEmailDone from '@/views/account/signup_by_email_done.vue'
import AccountUserPage from '@/views/account/userpage.vue'
import AccountUserFriendsPage from '@/views/account/friends.vue'
import AccountNotif from '@/views/account/notif.vue'
import AccountOAuth from '@/views/account/oauth.vue'
import AccountOAuthCheck from '@/views/account/oauth_check.vue'
import SettingUserinfoMe from '@/views/settings/userinfo/me.vue'
import SettingUserinfoUpload from '@/views/settings/userinfo/upload.vue'
import SettingSecurityOAuth from '@/views/settings/security/oauth.vue'
import Setting from '@/views/settings/setting.vue'
import Admin from '@/views/admin/admin.vue'
import AdminForumBoard from '@/views/admin/forum/board.vue'
import AdminForumTopic from '@/views/admin/forum/topic.vue'
import AdminCommonUser from '@/views/admin/common/user.vue'
import AdminCommonComment from '@/views/admin/common/comment.vue'
import AdminCommonManageLog from '@/views/admin/common/manage-log.vue'

// import ForumBoards from '@/views/forum/boards.vue'
import ForumMain from '@/views/forum/main.vue'
import ForumTopic from '@/views/forum/topic.vue'
import ForumTopicEdit from '@/views/forum/topic-edit.vue'

import Search from '@/views/search.vue'
import NotFoundComponent from '@/components/404.vue'

Vue.use(Router)

export function createRouter () {
    return new Router({
        mode: 'history',
        base: process.env.BASE_URL,
        routes: [
            // index
            {
                path: '/',
                name: 'index',
                component: ForumMain
            },

            // login
            {
                path: '/account/signin',
                name: 'account_signin',
                component: AccountSignin
            },
            // register
            {
                path: '/account/signup',
                name: 'account_signup',
                component: AccountSignup
            },
            // email
            {
                path: '/account/signup_by_email_done',
                name: 'account_signup_by_email_done',
                component: AccountSignupByEmailDone
            },
            // user page
            {
                path: '/user/:id(\\S+)',
                name: 'account_userpage',
                component: AccountUserPage
            },
            // friends list
            {
              path: '/user/friends/:id(\\S+)',
              name: 'account_user_friends_page',
              component: AccountUserFriendsPage
            },

            // notification
            {
                path: '/notifications',
                name: 'account_notif',
                component: AccountNotif
            },

            // main forum
            {
                path: '/forum',
                name: 'forum',
                redirect: { name: 'forum_main', params: { page: 1 } }
            },
            // {
            //     path: '/forum/boards',
            //     name: 'forum_boards',
            //     component: ForumBoards
            // },


           // main
            {
                path: '/r/:page(\\d+)',
                name: 'forum_main',
                component: ForumMain
            },
            // main
            {
                path: '/b/:id([a-fA-F0-9]+)/:page(\\d+)?/:name(.+)?',
                name: 'forum_board',
                component: ForumMain
            },
            // new post
            {
                path: '/topic/new',
                name: 'forum_topic_new',
                component: ForumTopicEdit
                // component: () => import(/* webpackChunkName: "topic-edit" */ '@/views/forum/topic-edit.vue')
            },
            // edit post
            {
                path: '/topic/edit/:id(\\S+)',
                name: 'forum_topic_edit',
                component: ForumTopicEdit
                // component: () => import(/* webpackChunkName: "topic-edit" */ '@/views/forum/topic-edit.vue')
            },
            // post detail
            {
                path: '/topic/:id([a-fA-F0-9]+)',
                name: 'forum_topic',
                component: ForumTopic
            },
            {
              path: '/setting',
              name: 'setting',
              redirect: { name: 'setting_user_me' },
              component: Setting
            },

           // my profile
            {
                path: '/setting/user/me',
                name: 'setting_user_me',
                component: SettingUserinfoMe
            },
            // upload
            {
                path: '/setting/user/upload',
                name: 'setting_user_upload',
                component: SettingUserinfoUpload
            },

            // admin
            {
                path: '/admin',
                name: 'admin',
                component: Admin
                // component: () => import(/* webpackChunkName: "admin" */ '@/views/admin/admin.vue')
            },
            // admin management
            {
                path: '/admin/forum/board',
                name: 'admin_forum_board',
                component: AdminForumBoard
                // component: () => import(/* webpackChunkName: "admin" */ '@/views/admin/forum/board.vue')
            },
            // admin management topic
            {
                path: '/admin/forum/topic/:page(\\d+)?/:name(.+)?',
                name: 'admin_forum_topic',
                component: AdminForumTopic
                // component: () => import(/* webpackChunkName: "admin" */ '@/views/admin/forum/topic.vue')
            },

            // admin management user
            {
                path: '/admin/common/user/:page(\\d+)?/:name(.+)?',
                name: 'admin_common_user',
                component: AdminCommonUser
                // component: () => import(/* webpackChunkName: "admin" */ '@/views/admin/common/user.vue')
            },
            // manage comments
            {
                path: '/admin/common/comment/:page(\\d+)?/:name(.+)?',
                name: 'admin_common_comment',
                component: AdminCommonComment
                // component: () => import(/* webpackChunkName: "admin" */ '@/views/admin/common/comment.vue')
            },
            // manage logs
            {
                path: '/admin/common/log/manage/:page(\\d+)?/:name(.+)?',
                name: 'admin_common_manage_log',
                component: AdminCommonManageLog
                // component: () => import(/* webpackChunkName: "admin" */ '@/views/admin/common/manage-log.vue')
            },

            {
                path: '*',
                component: NotFoundComponent
            },

            // search
            {
                path: '/search',
                name: 'search',
                component: Search
                // component: () => import(/* webpackChunkName: "search" */ '@/views/search.vue')
            },
            // OAuth
            {
                path: '/account/oauth',
                name: 'account_oauth',
                component: AccountOAuth
            },

            // OAuth Check
            {
                path: '/account/oauth_check',
                name: 'account_oauth_check',
                component: AccountOAuthCheck
            }
        ]
    })
}
