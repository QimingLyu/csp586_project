# Spring22 CSP586 project 

## step1 
```shell
cd {project_root}

docker-compose up
```

step2
```shell
docker cp {project_root}/nginx/default.conf csp586_nginx:/etc/nginx/conf.d/default.conf

docker restart csp586_nginx
```