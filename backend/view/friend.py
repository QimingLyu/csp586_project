from typing import Dict, List

from slim.base.sqlquery import SQLValuesToWrite, DataRecord
from slim.retcode import RETCODE
from slim.support.peewee import PeeweeView
from slim.utils import to_hex, to_bin

from model.friend import Friend
from view import route
from view.user import UserViewMixin


@route('friend')
class FriendView(PeeweeView, UserViewMixin):
    model = Friend

    async def before_insert(self, raw_post: Dict, values: SQLValuesToWrite):
        values['user_id'] = self.current_user.id

    @route.interface('POST')
    async def new(self):
        follow_id = None
        datas = await self.post_data()
        if 'follow_id' in datas:
            follow_id = datas['follow_id']

        count = Friend.select().where(Friend.user_id == self.current_user.id, Friend.follow_id == to_bin(follow_id)).count()
        if count > 0:
            self.finish(RETCODE.FAILED)
        return await super().new()

    @route.interface('POST')
    async def delete(self):
        self.finish(RETCODE.FAILED)

    @route.interface('POST')
    async def unfollow(self):
        user_id = to_hex(self.current_user.id)
        data = self.params
        if 'follow_id' in data:
            Friend.unfollow(user_id, data['follow_id'])
            return self.finish(RETCODE.SUCCESS)

        self.finish(RETCODE.FAILED)

    @route.interface('GET')
    async def list(self):
        follow_id = None
        user_id = None
        page = 1
        size = 10
        if 'page' in self.params and int(self.params['page']) > 1:
            page = int(self.params['page'])
        if 'size' in self.params and int(self.params['size']) > 0:
            size = int(self.params['size'])
        if 'user_id' in self.params:
            user_id = self.params['user_id']
        if 'follow_id' in self.params:
            follow_id = self.params['follow_id']

        if user_id is None and follow_id is None:
            return self.finish(RETCODE.FAILED)

        result = []
        cursor = Friend.list(user_id, follow_id, page, size)
        if cursor:
            fetchall = cursor.fetchall()
            for item in fetchall:
                result.append({'user_id': to_hex(item[0]), 'nickname': item[1], 'follow_id': to_hex(item[2]),
                               'follow_nickname': item[3]})

        self.finish(RETCODE.SUCCESS, result)

    @route.interface('GET')
    async def count(self):
        values = self.params

        if 'user_id' in values:
            user_id = values['user_id']
            cursor = Friend.count_by_user_id(user_id)
            if cursor:
                one = cursor.fetchone()
            self.finish(RETCODE.SUCCESS, one[0])
            return

        if 'follow_id' in values:
            follow_id = values['follow_id']
            cursor = Friend.count_by_follow_id(follow_id)
            one = cursor.fetchone()
            self.finish(RETCODE.SUCCESS, one[0])
            return

        self.finish(RETCODE.FAILED)
