from slim.base.permission import Ability, A
from slim.base.sqlquery import SQLQueryInfo

from permissions.roles import *


def func(ability: Ability, user, query: 'SQLQueryInfo'):
    query.select.add('user_id')
    query.select.add('follow_id')


banned_user.add_query_condition((A.ALL,), 'friend', func=func)
inactive_user.add_query_condition((A.ALL,), 'friend', func=func)
