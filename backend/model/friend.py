from peewee import BlobField, SQL
from slim.base.user import BaseUser

from model import db, BaseModel


class Friend(BaseModel):
    id = BlobField(primary_key=True, constraints=[SQL("DEFAULT int2bytea(nextval('id_gen_seq'))")])
    user_id = BlobField(index=True, null=True, default=None)
    follow_id = BlobField(index=True, null=True, default=None)

    class Meta:
        db_table = 'friend'

    @classmethod
    def count_by_user_id(cls, user_id):
        return db.execute_sql("SELECT count(1) from friend where user_id = decode('" + user_id + "', 'hex')")

    @classmethod
    def count_by_follow_id(cls, follow_id):
        return db.execute_sql("SELECT count(1) from friend where follow_id = decode('" + follow_id + "', 'hex')")

    @classmethod
    def unfollow(cls, user_id, follow_id):
        return db.execute_sql("DELETE from friend where follow_id = decode('" + follow_id + "', 'hex') and user_id = decode('" + user_id + "', 'hex')")

    @classmethod
    def list(cls, user_id, follow_id, page, size):
        sql = """
            SELECT
                f.user_id,
                u1.nickname,
                f.follow_id,
                fo.nickname as follow_nickname
            FROM
                friend f
                LEFT JOIN "user" u1 ON u1."id" = f.user_id
                LEFT JOIN "user" fo ON fo."id" = f.follow_id 
            WHERE
                f.user_id = decode(%s, 'hex')
            LIMIT %s offset %s
        """

        sql2 = """
            SELECT
                f.user_id,
                u1.nickname,
                f.follow_id,
                fo.nickname as follow_nickname
            FROM
                friend f
                LEFT JOIN "user" u1 ON u1."id" = f.user_id
                LEFT JOIN "user" fo ON fo."id" = f.follow_id 
            WHERE
                f.follow_id = decode(%s, 'hex')
            LIMIT %s offset %s
        """

        if user_id:
            return db.execute_sql(sql, (user_id, size, (page - 1) * size))
        else:
            return db.execute_sql(sql2, (follow_id, size, (page - 1) * size))
